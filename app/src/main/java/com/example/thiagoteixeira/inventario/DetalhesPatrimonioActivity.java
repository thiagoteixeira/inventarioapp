package com.example.thiagoteixeira.inventario;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandlerHC4;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DetalhesPatrimonioActivity extends AppCompatActivity {

    String numPatrimonio, descricao, nf, dtCadastro, cadastradoPor, vlrAquisicao, unidade, setor, fornecedor, fabricante, modelo,
    categoria, tipo_patrimonio, situacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_patrimonio);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Bundle bundle = getIntent().getExtras();
        String patrimonio = bundle.getString("patrimonio");

        detalharPatrimonio(patrimonio);
    }

    @Override
    protected void onActivityResult(int requestCod, int resultCod, Intent data)
    {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCod,resultCod,data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this,"teste", Toast.LENGTH_LONG).show();
            } else {
                detalharPatrimonio(result.getContents());
            }
        } else {
            super.onActivityResult(requestCod, resultCod, data);
        }
    }

    private void detalharPatrimonio(String patrimonio)
    {
        String url = "http://thiago.dantondietze.com/patrimonio/" + patrimonio;

        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"UTF-8"));
            String result = reader.readLine();

            if (result == null || result == "" || result.isEmpty()) {
                TextView tvResposta = (TextView) findViewById(R.id.tvRespostaPatrimonio);
                tvResposta.setText("Nenhum patrimônio encontrado!");
            } else {

                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objJson = (JSONObject) jsonArray.get(i);

                    unidade = objJson.getString("UNIDADE");
                    setor = objJson.getString("SETOR");
                    categoria = objJson.getString("CATEGORIA");
                    tipo_patrimonio = objJson.getString("TIPO_PATRIMONIO");
                    fornecedor = objJson.getString("FORNECEDOR");
                    fabricante = objJson.getString("FABRICANTE");
                    modelo = objJson.getString("MODELO");
                    numPatrimonio = objJson.getString("PATRIMONIO");
                    descricao = objJson.getString("DESCRICAO");
                    nf = (objJson.getString("NF") == "null") ? " -" : objJson.getString("NF");
                    dtCadastro = objJson.getString("DT_CADASTRO");
                    cadastradoPor = objJson.getString("USUARIO_CAD");
                    vlrAquisicao = (objJson.getString("VALOR_AQUISICAO") == "null") ? " - " : objJson.getString("VALOR_AQUISICAO");
                }

                ArrayList<String> detalhes = new ArrayList<>();
                detalhes.add("Unidade:" + unidade);
                detalhes.add("Setor:" + setor);
                detalhes.add("Categoria:" + categoria);
                detalhes.add("Tipo:" + tipo_patrimonio);
                detalhes.add("Fornecedor:" + fornecedor);
                detalhes.add("Fabricante" + fabricante);
                detalhes.add("Modelo:" + modelo);
                detalhes.add("Valor:" + vlrAquisicao);
                detalhes.add("Descrição: " + descricao);
                detalhes.add("Nota Fiscal: " + nf);
                detalhes.add("Valor:" + vlrAquisicao);
                detalhes.add("Cadastrado em: " + dtCadastro);
                detalhes.add("Cadastrado por: " + cadastradoPor);

                TextView tvNumPatrimonio = (TextView) findViewById(R.id.tvNumPatrimonio);
                tvNumPatrimonio.setText(numPatrimonio);

                ListView lv = (ListView) findViewById(R.id.lvDetalhesPatrimonio);
                ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,detalhes);

                lv.setAdapter(adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
