package com.example.thiagoteixeira.inventario;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buscarPatrimonio(View view)
    {
        Intent intent = new Intent(this,DetalhesPatrimonioActivity.class);

        TextView numPatrimonio = (TextView) findViewById(R.id.tbPatrimonio);
        Bundle bundle = new Bundle();
        bundle.putString("patrimonio",numPatrimonio.getText().toString());
        intent.putExtras(bundle);

        startActivity(intent);
    }

    public void escanearCodigoBarra(View view)
    {
        final Activity activity = this;
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCod, int resultCod, Intent data)
    {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCod,resultCod,data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this,"teste", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(this,DetalhesPatrimonioActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("patrimonio",result.getContents());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            super.onActivityResult(requestCod, resultCod, data);
        }
    }

    /*
    public void enviarFeedback(View view)
    {
        Intent i = new Intent(Intent.ACTION_SEND);

        EditText email = (EditText) findViewById(R.id.etEmail);
        EditText msg = (EditText) findViewById(R.id.etMsg);

        String[] emailTo = {"thiagohteixeira01@gmail.com","eldfarias@gmail.com"};
        String textoMsg = msg.getText().toString();

        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailTo);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
        emailIntent.putExtra(Intent.EXTRA_TEXT,textoMsg);

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviando feedback..."));
            finish();
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "Nenhum email do cliente configurado!", Toast.LENGTH_SHORT).show();
        }
    }*/
}
